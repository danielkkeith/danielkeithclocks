package util;

import static org.junit.Assert.*;

import org.junit.Test;

import clock.ClockModel;

public class ClocksTests {

	@Test
	public void testClockModel() {
		fail("Not yet implemented");
	}

	@Test
	public void testClockModelIntIntInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetTime() {
		// this is a comment
		ClockModel c = new ClockModel();
		c.setHours(2);
		c.setMinutes(3);
		c.setSeconds(4);
		assertEquals(2, c.getHours());
		assertEquals(3, c.getMinutes());
		assertEquals(4, c.getSeconds());
	}

	@Test
	public void testIncrementSeconds() {
		ClockModel d = new ClockModel(12, 0, 0);
		d.incrementSeconds();
		assertEquals(1, d.getSeconds());
		assertEquals(0, d.getMinutes()); 
		assertEquals(12, d.getHours());
		
		//only minutes rolls over
		d.setHours(12);
		d.setMinutes(0);
		d.setSeconds(59);
		d.incrementSeconds();
		assertEquals(0, d.getSeconds());
		assertEquals(1, d.getMinutes());
		assertEquals(12, d.getHours());
		
		//both minutes and hours roll over
		d.setHours(12);
		d.setMinutes(59);
		d.setSeconds(59);
		d.incrementSeconds();
		assertEquals(0, d.getSeconds());
		assertEquals(0, d.getMinutes());
		assertEquals(1, d.getHours());
	}

	@Test
	public void testIncrementMinutes() {
		fail("Not yet implemented");
	}

}
